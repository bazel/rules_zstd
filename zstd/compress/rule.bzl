load("//zstd/dictionary:defs.bzl", "ZstdDictionaryInfo")

visibility("//zstd/...")

DOC = """Compresses a file with the Zstandard algorithm, optionally it can take in a dictionary.

```py
zstd_compress(
    name = "compress",
    src = "some/file.txt",
    level = 1-22,
    dictionary = "optional/path/to/dictionary",
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "A file to compress with the Zstandard algorithm.",
        mandatory = True,
        allow_single_file = True,
    ),
    "level": attr.int(
        doc = "The compression level.",
        mandatory = False,
        default = 3,
        values = [l for l in range(1, 23)],
    ),
    "dictionary": attr.label(
        doc = "A dictionary that has been pre-trained with a relevant dataset for the `src` to be compressed.",
        mandatory = False,
        allow_single_file = True,
        default = None,
    ),
}

def implementation(ctx):
    zstd = ctx.toolchains["//zstd/toolchain/zstd:type"]
    output = ctx.actions.declare_file("{}.zst".format(ctx.file.src.basename))
    inputs = [ctx.file.src]
    providers = []

    args = ctx.actions.args()
    if ctx.attr.dictionary and ZstdDictionaryInfo in ctx.attr.dictionary:
        info = ctx.attr.dictionary[ZstdDictionaryInfo]
        args.add("-D").add(info.file)
        args.add("--dictID").add(info.id)
        inputs.append(info.file)
        providers.append(info)

    elif ctx.file.dictionary:
        args.add("-D").add(ctx.file.dictionary)
        inputs.append(ctx.file.dictionary)

    if ctx.attr.level >= 19:
        args.add("--ultra")

    args.add("-zfk").add(ctx.file.src)
    args.add("-{}".format(ctx.attr.level))

    args.add("-o").add(output)

    ctx.actions.run(
        outputs = [output],
        inputs = inputs,
        arguments = [args],
        executable = zstd.run,
        toolchain = "//zstd/toolchain/zstd:type",
        mnemonic = "ZstdCompress",
        progress_message = "Compressing %{input} to %{output}",
    )

    default = DefaultInfo(files = depset([output]))
    providers.append(default)
    return providers

zstd_compress = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//zstd/toolchain/zstd:type"],
)

compress = zstd_compress
