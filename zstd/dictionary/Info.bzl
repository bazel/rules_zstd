load("@bazel_skylib//lib:types.bzl", "types")

visibility("//zstd/...")

def init(id, srcs, file):
    """
    Initializes a `ZstdDictionaryInfo` provider.

    Args:
      id: A 1-4 byte integer.
      srcs: A dependency set of files that train the dictionary.
      file: The dictionary.

    Returns:
      A mapping of keywords for the `zstd_dictionary_info` raw constructor.
    """
    if not types.is_int(id):
        fail("`ZstdDictionaryInfo.id` must be a `int`: {}".format(id))

    if not types.is_depset(srcs):
        fail("`ZstdDictionaryInfo.srcs` must be a `depset`: {}".format(srcs))

    if type(file) != "File":
        fail("`ZstdDictionaryInfo.file` must be a `file`: {}".format(file))

    return {"id": id, "srcs": srcs, "file": file}

ZstdDictionaryInfo, zstd_dictionary_info = provider(
    "A zstd dictionary.",
    fields = ["id", "srcs", "file"],
    init = init,
)

Info = ZstdDictionaryInfo

info = zstd_dictionary_info
