load(":Info.bzl", "ZstdDictionaryInfo")
load("@aspect_bazel_lib//lib:strings.bzl", "ord")

visibility("//zstd/...")

DOC = """Create a ZStandard dictionary to be used to compress files.

```py
zstd_dictionary(
    name = "dictionary",
    srcs = [
        //some/file/path:text.txt,
        //some/file/path:goodbye-world.txt,
        //some/file/path:hello-world.txt,
        //some/file/path:goodbye-text.txt,
        //some/file/path:hello-text.txt,
        //some/file/path:text-test.txt,
        //some/file/path:test-text.txt
    ],
    id = "A-string",
)
```
"""

ATTRS = {
    "srcs": attr.label_list(
        doc = "A list of files that Zstandard uses to create a dictionary to speed up file compression.",
        allow_files = True,
    ),
    "id": attr.int(
        doc = "A optional integer for manually setting the Dictionary ID.",
        mandatory = False,
    ),
}

def _djb2(s):
    hash = 5381
    for c in s.elems():
        hash = (((hash << 5) + hash) + ord(c)) & 0xFFFFFFFF
    return hash

def implementation(ctx):
    zstd = ctx.toolchains["//zstd/toolchain/zstd:type"]

    srcs = ctx.files.srcs
    id = ctx.attr.id or _djb2(str(ctx.label))
    output = ctx.actions.declare_file(ctx.attr.name)
    args = ctx.actions.args()
    args.add("--train")
    args.add("--dictID").add(id)
    args.add("-f").add_all(srcs)
    args.add("-o").add(output)

    ctx.actions.run(
        outputs = [output],
        inputs = srcs,
        arguments = [args],
        executable = zstd.run,
        toolchain = "//zstd/toolchain/zstd:type",
        mnemonic = "ZstdDictionary",
        progress_message = "Creating dictionary from %{input}",
    )

    return DefaultInfo(files = depset([output])), ZstdDictionaryInfo(srcs = depset(srcs), id = id, file = output)

zstd_dictionary = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//zstd/toolchain/zstd:type"],
    provides = [ZstdDictionaryInfo],
)

dictionary = zstd_dictionary
