load(":Info.bzl", _Info = "Info")
load(":rule.bzl", _dictionary = "zstd_dictionary")

visibility("public")

zstd_dictionary = _dictionary

ZstdDictionaryInfo = _Info
