load("//zstd/dictionary:defs.bzl", "ZstdDictionaryInfo")

visibility("//zstd/...")

DOC = """Decompresses a ZStandard compressed file, optionally it can take in a dictionary.

```py
zstd_decompress(
    name = "decompress",
    src = ":archive.zstd",
    dictionary = "optional/path/to/dictionary",
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "Decompresses a Zstandard compressed file.",
        mandatory = True,
        allow_single_file = [".zst"],
    ),
    "dictionary": attr.label(
        doc = "A dictionary that has been pre-trained with a relevant dataset for the `src` to be compressed.",
        mandatory = False,
        allow_single_file = True,
        default = None,
    ),
}

def implementation(ctx):
    zstd = ctx.toolchains["//zstd/toolchain/zstd:type"]
    output = ctx.actions.declare_file(ctx.file.src.basename.removesuffix(".zst"))
    inputs = [ctx.file.src]
    providers = []
    args = ctx.actions.args()

    if ctx.attr.dictionary and ZstdDictionaryInfo in ctx.attr.dictionary:
        info = ctx.attr.dictionary[ZstdDictionaryInfo]
        args.add("-D").add(info.file)
        args.add("--dictID").add(info.id)
        inputs.append(info.file)
        providers.append(info)

    elif ctx.file.dictionary:
        args.add("-D").add(ctx.file.dictionary)
        inputs.append(ctx.file.dictionary)

    elif ZstdDictionaryInfo in ctx.attr.src:
        info = ctx.attr.src[ZstdDictionaryInfo]
        args.add("-D").add(info.file)
        args.add("--dictID").add(info.id)
        inputs.append(info.file)
        providers.append(info)

    args.add("-dfk").add(ctx.file.src)
    args.add("-o").add(output)

    ctx.actions.run(
        outputs = [output],
        inputs = inputs,
        arguments = [args],
        executable = zstd.run,
        toolchain = "//zstd/toolchain/zstd:type",
        mnemonic = "ZstdCompress",
        progress_message = "Decompressing %{input} to %{output}",
    )

    default = DefaultInfo(files = depset([output]))
    providers.append(default)
    return providers

zstd_decompress = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//zstd/toolchain/zstd:type"],
)

decompress = zstd_decompress
